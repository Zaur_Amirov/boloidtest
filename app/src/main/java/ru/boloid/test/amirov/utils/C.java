package ru.boloid.test.amirov.utils;

/**
 * Created by zauramirov on 30.07.15.
 */
public class C {
    public static String HTTP_URL = "http://test.boloid.com:9000/";
    public static String DOWNLOAD_TASKS = "tasks";
    public static int TIMEOUT_SERVER_RESPONSE = 20000;

    public static String ID = "ID";
    public static String TITLE = "title";
    public static String DATE = "date";

    public static String TEXT = "text";
    public static String LONGTEXT = "longText";
    public static String DURATION_LIMIT_TEXT = "durationLimitText";
    public static String LOCATION = "location";
    public static String PRICE = "price";
    public static String LOCATION_TEXT = "locationText";
    public static String LAT = "lat";
    public static String LON = "lon";
    public static String ZOOM_LEVEL = "zoomLevel";
    public static  String PRICES = "prices";
    public static String DESCRIPTION = "description";
    public static String TRANSLATION = "translation";
    public static String TASKS = "tasks";
}
