package ru.boloid.test.amirov;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import ru.boloid.test.amirov.utils.C;
import ru.boloid.test.amirov.utils.Utils;

/**
 * Created by Amirov Zaur on 31.07.15.
 */
public class InformationBalloonActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_layout);
        Intent intent = getIntent();
        TextView tv_text = (TextView)findViewById(R.id.tv_text);
        tv_text.setText(intent.getStringExtra(C.TEXT));
        TextView tv_longtext = (TextView)findViewById(R.id.tv_longtext);
        tv_longtext.setText(intent.getStringExtra(C.LONGTEXT));

        TextView tv_prices = (TextView)findViewById(R.id.tv_prices);
        tv_prices.setText(intent.getStringExtra(C.PRICES));

        TextView tv_date = (TextView)findViewById(R.id.tv_date);
        tv_date.setText(Utils.dateToString(intent.getLongExtra(C.DATE, 0)));

        TextView tv_locationtext = (TextView)findViewById(R.id.tv_locationtext);
        tv_locationtext.setText(intent.getStringExtra(C.LOCATION_TEXT));

    }
}
