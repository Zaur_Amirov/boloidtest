package ru.boloid.test.amirov;

import android.app.Application;

/**
 * Created by Amirov Zaur on 30.07.15.
 */
public class App extends Application {
    private static App instance;
    public static App getInstance() {
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        setInstance(this);
    }

    private static void setInstance(final App instance) {
        App.instance = instance;
    }
}
