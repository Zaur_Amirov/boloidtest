package ru.boloid.test.amirov.tasks;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;


import ru.boloid.test.amirov.DownloadTasksTaskCallback;
import ru.boloid.test.amirov.R;
import ru.boloid.test.amirov.utils.C;
import ru.boloid.test.amirov.utils.Utils;


/**
 * Created by Amirov Zaur on 30.07.15.
 */
public class DownloadTasksTask extends AsyncTask {

    private final DownloadTasksTaskCallback callback;
    private final String TAG = "DownloadTasksTask";

    public DownloadTasksTask(DownloadTasksTaskCallback callback) {
        this.callback = callback;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        String response = null;
        URL url = null;
        try {
            url = new URL(C.HTTP_URL + C.DOWNLOAD_TASKS);

            if (Utils.isNetworkEnabled()) {
                response = Utils.request(url);

                if (response == null || response.isEmpty()) {
                    callback.error(Utils.getString(R.string.noServerResponse));
                } else {
                    Utils.logD(TAG, response);
                    final JSONObject jsonObject = new JSONObject(response);
                    final JSONArray jsonOArray = jsonObject.getJSONArray(C.TASKS);
                    callback.tasks(Utils.parcingJsonTask(jsonOArray));
                }
            } else
                callback.error(Utils.getString(R.string.noInternet));
        } catch (JSONException e) {

        }
        catch (MalformedURLException e) {

        }
        return null;
    }


}
