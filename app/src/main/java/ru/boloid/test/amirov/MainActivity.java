package ru.boloid.test.amirov;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import java.util.ArrayList;

import ru.boloid.test.amirov.model.Price;
import ru.boloid.test.amirov.model.Task;
import ru.boloid.test.amirov.tasks.DownloadTasksTask;
import ru.boloid.test.amirov.utils.C;
import ru.boloid.test.amirov.utils.Utils;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;


public class MainActivity extends Activity implements DownloadTasksTaskCallback, OnBalloonListener {
    private ArrayList<Task> tasks;
    private String TAG = "MainActivity";
    private MapController mMapController;
    private OverlayManager mOverlayManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        DownloadTasksTask task = new DownloadTasksTask(this);
        task.execute();

        final MapView mapView = (MapView) findViewById(R.id.map);

        mMapController = mapView.getMapController();

        mOverlayManager = mMapController.getOverlayManager();
        // Disable determining the user's location
        mOverlayManager.getMyLocation().setEnabled(false);
        mMapController.setZoomCurrent(1);

        ImageButton imbtn_update = (ImageButton)findViewById(R.id.imbtn_update);
        imbtn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadTasksTask task = new DownloadTasksTask(MainActivity.this);
                task.execute();
            }
        });

    }

    @Override
    public void error(final String errorMessage) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Utils.showToast(errorMessage);
            }
        });
    }

    @Override
    public void tasks(final ArrayList<Task> tasks) {
        this.tasks = tasks;
        for (int i = 0; i < this.tasks.size(); i++) {
            Utils.logD(TAG, C.ID + " = " + tasks.get(i).getId());
            Utils.logD(TAG, C.TITLE + " = " + tasks.get(i).getTitle());
            Utils.logD(TAG, C.TEXT + " = " + tasks.get(i).getText());
            Utils.logD(TAG, C.LONGTEXT + " = " + tasks.get(i).getLongText());
            Utils.logD(TAG, C.DURATION_LIMIT_TEXT + " = " + tasks.get(i).getDurationLimitText());
            Utils.logD(TAG, C.PRICE + " = " + tasks.get(i).getPrice());
            Utils.logD(TAG, C.LOCATION_TEXT + " = " + tasks.get(i).getLocationText());
            Utils.logD(TAG, C.LAT + " = " + tasks.get(i).getLat());
            Utils.logD(TAG, C.LON + " = " + tasks.get(i).getLon());

            ArrayList<Price> prices = tasks.get(i).getPrices();

            for (int j = 0; j < prices.size(); j++) {

                Utils.logI(TAG, C.PRICE + " = " + prices.get(j).getPrice() + " " + C.DESCRIPTION + " = " + prices.get(j).getDescription());
            }

            Utils.logD(TAG, C.ZOOM_LEVEL + " = " + tasks.get(i).getZoomLevel());
            Utils.logD(TAG, C.TRANSLATION + " = " + tasks.get(i).isTranslation());
        }
        showObject();
    }

    public void showObject() {

        // Create a layer of objects for the map
        Overlay overlay = new Overlay(mMapController);

        for (int i = 0; i < tasks.size(); i++) {
            // Create an object for the layer
            OverlayItem item = new OverlayItem(new GeoPoint(tasks.get(i).getLat(), tasks.get(i).getLon()), getResources().getDrawable(R.drawable.a));
            // Create a balloon model for the object
            BalloonItem balloonItem = new BalloonItem(this, item.getGeoPoint());
            balloonItem.setText(tasks.get(i).getTitle());


            balloonItem.setOnBalloonListener(this);
            // Add the balloon model to the object
            item.setBalloonItem(balloonItem);
            // Add the object to the layer
            overlay.addOverlayItem(item);
        }


        // Add the layer to the map
        mOverlayManager.addOverlay(overlay);

    }

    @Override
    public void onBalloonViewClick(BalloonItem balloonItem, View view) {
        balloonItem.getGeoPoint();
        for (int i = 0; i < tasks.size(); i++)
            if (tasks.get(i).isThisPoint(balloonItem.getGeoPoint())) {
                Intent intent = new Intent(this, InformationBalloonActivity.class);
                intent.putExtra(C.TEXT, tasks.get(i).getText());
                intent.putExtra(C.LONGTEXT, tasks.get(i).getLongText());
                String prices = null;
                for (int j = 0; j < tasks.get(i).getPrices().size(); j++) {
                    if (prices != null)
                        prices = prices + "  " + (String.valueOf(tasks.get(i).getPrices().get(j).getPrice()));
                    else prices = (String.valueOf(tasks.get(i).getPrices().get(j).getPrice()));
                }
                intent.putExtra(C.PRICES, prices);
                intent.putExtra(C.DATE, tasks.get(i).getDate());
                intent.putExtra(C.LOCATION_TEXT, tasks.get(i).getLocationText());
                startActivity(intent);
            }
    }

    @Override
    public void onBalloonShow(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonHide(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonAnimationStart(BalloonItem balloonItem) {

    }

    @Override
    public void onBalloonAnimationEnd(BalloonItem balloonItem) {

    }
}
