package ru.boloid.test.amirov.model;

import java.util.ArrayList;

import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by Amirov Zaur on 30.07.15.
 */
public class Task {
    private int id;
    private String title;
    private long date;
    private String text;
    private String longText;
    private String durationLimitText;
    private int price;
    private String locationText;
    private double lat;
    private double lon;
    private int zoomLevel;
    private ArrayList<Price> prices;
    private boolean translation;

    public Task() {

    }

    public Task(final int id, final String title, final long date, final String text, final String longText, final String durationLimitText,
                final int price, final String locationText, final double lat, final double lon, final int zoomLevel,
                final ArrayList<Price> prices, final boolean translation) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.text = text;
        this.longText = longText;
        this.durationLimitText = durationLimitText;
        this.price = price;
        this.locationText = locationText;
        this.lat = lat;
        this.lon = lon;
        this.zoomLevel = zoomLevel;
        this.prices = prices;
        this.translation = translation;

    }


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public long getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public String getLongText() {
        return longText;
    }

    public String getDurationLimitText() {
        return durationLimitText;
    }

    public int getPrice() {
        return price;
    }

    public String getLocationText() {
        return locationText;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getZoomLevel() {
        return zoomLevel;
    }

    public ArrayList<Price> getPrices() {
        return prices;
    }

    public boolean isTranslation() {
        return translation;
    }

    public boolean isThisPoint(GeoPoint geoPoint){
        GeoPoint geoPointThis = new GeoPoint(lat,lon);
        return geoPointThis.equals(geoPoint);
    }
}
