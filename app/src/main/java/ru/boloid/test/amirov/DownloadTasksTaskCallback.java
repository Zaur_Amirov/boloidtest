package ru.boloid.test.amirov;

import java.util.ArrayList;

import ru.boloid.test.amirov.model.Task;

/**
 * Created by zauramirov on 30.07.15.
 */
public interface DownloadTasksTaskCallback {
    public void error(final String errorMessage);
    public void tasks(final ArrayList<Task> tasks);
}
