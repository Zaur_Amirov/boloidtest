package ru.boloid.test.amirov.model;

/**
 * Created by Amirov Zaur on 30.07.15.
 */
public class Price {
    private int price;
    private String description;

    public Price() {
    }
    public Price(final int price, final String description){
        this.price = price;
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }
}
