package ru.boloid.test.amirov.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import ru.boloid.test.amirov.App;
import ru.boloid.test.amirov.BuildConfig;
import ru.boloid.test.amirov.model.Price;
import ru.boloid.test.amirov.model.Task;

/**
 * Created by Amirov Zaur on 30.07.15.
 */
public class Utils {

    private static final String TAG = "BoloidTest";

    public static String request(final URL url) {
        String result = null;
        BufferedReader reader = null;
        DataOutputStream writer = null;


        try {
            // for https replace HttpURLConnection - HttpsURLConnection
            final HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();

            httpConnection.setRequestMethod("GET");

            httpConnection.setReadTimeout(C.TIMEOUT_SERVER_RESPONSE);


            // Get Response
            reader = new BufferedReader(new InputStreamReader(
                    httpConnection.getInputStream(), Charset.forName("UTF-8")));
            final StringBuffer response = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            Utils.logD(TAG, "resultJSON = " + response.toString());
            result = response.toString();
        } catch (IOException ex) {

            Utils.log(ex);
        } finally {
            Utils.close(reader);
            Utils.close(writer);
        }
        return result;
    }

    /**
     * Check network connection
     *
     * @return
     */
    public static boolean isNetworkEnabled() {
        final ConnectivityManager connMan = (ConnectivityManager) App
                .getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = connMan.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    /**
     * Print exception stack trace
     *
     * @param error
     */
    public static void log(final Exception error) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, "Error: ", error);
        }
    }

    /**
     * Log.d
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

    /**
     * DBGE == Log errors(Log.e)
     *
     * @param tag
     * @param msg
     */
    public static void logE(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Error!" : msg);
        }
    }

    /**
     * Log.i
     *
     * @param tag
     * @param msg
     */
    public static void logI(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Info" : msg);
        }
    }

    /**
     * @param obj - instance of {@link Closeable} to close gracefully
     */
    public static void close(final Closeable obj) {
        if (obj != null) {
            try {
                obj.close();
            } catch (IOException e) {
                Utils.logE(TAG, e.getMessage());
            }
        }
    }

    /**
     * Get String by id
     *
     * @param id
     * @return
     */
    public static String getString(final int id) {
        return App.getInstance().getResources().getString(id);
    }

    public static void showToast(final String message) {
        Toast toast = Toast.makeText(App.getInstance().getApplicationContext(),
                message,
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static ArrayList<Price> parcingJsonPrice(final JSONArray json) throws JSONException {
        ArrayList<Price> result = new ArrayList<Price>();
        for (int i = 0; i < json.length(); i++) {
            JSONObject object = json.getJSONObject(i);
            result.add(new Price(object.getInt(C.PRICE), object.getString(C.DESCRIPTION)));
        }
        return result;

    }

    public static ArrayList<Task> parcingJsonTask(final JSONArray json) throws JSONException {
        ArrayList<Task> result = new ArrayList<Task>();

        for (int i = 0; i < json.length(); i++) {
            JSONObject object = json.getJSONObject(i);
            int id = object.getInt(C.ID);
            String title = object.getString(C.TITLE);
            Long date = object.getLong(C.DATE);
            String text = object.getString(C.TEXT);
            String longText = object.getString(C.LONGTEXT);
            String durationLimitText = object.getString(C.DURATION_LIMIT_TEXT);
            int price = object.getInt(C.PRICE);
            String locationText = object.getString(C.LOCATION_TEXT);
            JSONObject jsonLocation = object.getJSONObject(C.LOCATION);
            double lat = jsonLocation.getDouble(C.LAT);
            double lon = jsonLocation.getDouble(C.LON);
            int zoomLLevel = object.getInt(C.ZOOM_LEVEL);
            JSONArray jsonPrices = object.getJSONArray(C.PRICES);
            boolean translation = object.getBoolean(C.TRANSLATION);


            result.add(new Task(id, title, date, text, longText, durationLimitText, price, locationText, lat, lon, zoomLLevel,
                    parcingJsonPrice(jsonPrices), translation));
        }

        return result;
    }

    public static String dateToString(final Long date) {
        return DateFormat.format("dd.MM.yyyy", date).toString();

    }
}
